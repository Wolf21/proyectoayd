-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: sweis
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrador`
--

DROP TABLE IF EXISTS `administrador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrador` (
  `correo` varchar(30) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `contrasena` varchar(30) NOT NULL,
  PRIMARY KEY (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrador`
--

LOCK TABLES `administrador` WRITE;
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuesta`
--

DROP TABLE IF EXISTS `encuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuesta` (
  `id_encuesta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `objetivo` text NOT NULL,
  `fecha_creación` date NOT NULL,
  `fecha_publicacion` date NOT NULL,
  `fecha_cierre` date NOT NULL,
  `población` int(11) NOT NULL,
  PRIMARY KEY (`id_encuesta`),
  UNIQUE KEY `FK_id_poblacion` (`población`),
  CONSTRAINT `encuesta_ibfk_1` FOREIGN KEY (`población`) REFERENCES `poblacion` (`id-poblacion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuesta`
--

LOCK TABLES `encuesta` WRITE;
/*!40000 ALTER TABLE `encuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuesta_contestada`
--

DROP TABLE IF EXISTS `encuesta_contestada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuesta_contestada` (
  `id_resp_enc` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_contestada` date NOT NULL,
  `encuestado` varchar(50) NOT NULL,
  PRIMARY KEY (`id_resp_enc`),
  KEY `encuestado` (`encuestado`),
  CONSTRAINT `encuesta_contestada_ibfk_1` FOREIGN KEY (`encuestado`) REFERENCES `encuestado` (`correo_electronico`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuesta_contestada`
--

LOCK TABLES `encuesta_contestada` WRITE;
/*!40000 ALTER TABLE `encuesta_contestada` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuesta_contestada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestado`
--

DROP TABLE IF EXISTS `encuestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestado` (
  `correo_electronico` varchar(50) NOT NULL,
  `poblacion` int(11) NOT NULL,
  PRIMARY KEY (`correo_electronico`),
  UNIQUE KEY `FK_id_poblacion` (`poblacion`),
  CONSTRAINT `encuestado_ibfk_1` FOREIGN KEY (`poblacion`) REFERENCES `poblacion` (`id-poblacion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestado`
--

LOCK TABLES `encuestado` WRITE;
/*!40000 ALTER TABLE `encuestado` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion`
--

DROP TABLE IF EXISTS `opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `pregunta` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion`),
  UNIQUE KEY `FK_id_pregunta` (`pregunta`),
  CONSTRAINT `opcion_ibfk_1` FOREIGN KEY (`pregunta`) REFERENCES `pregunta` (`id_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion`
--

LOCK TABLES `opcion` WRITE;
/*!40000 ALTER TABLE `opcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_respuesta`
--

DROP TABLE IF EXISTS `opcion_respuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion_respuesta` (
  `id_opcion_respuesta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `pregunta_contestada` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion_respuesta`),
  UNIQUE KEY `FK_id_pregunta_contestada` (`pregunta_contestada`),
  CONSTRAINT `opcion_respuesta_ibfk_1` FOREIGN KEY (`pregunta_contestada`) REFERENCES `pregunta_contestada` (`id_pregunta_contestada`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_respuesta`
--

LOCK TABLES `opcion_respuesta` WRITE;
/*!40000 ALTER TABLE `opcion_respuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `opcion_respuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poblacion`
--

DROP TABLE IF EXISTS `poblacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poblacion` (
  `id-poblacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `tamaño` int(11) NOT NULL,
  PRIMARY KEY (`id-poblacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poblacion`
--

LOCK TABLES `poblacion` WRITE;
/*!40000 ALTER TABLE `poblacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `poblacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pregunta`
--

DROP TABLE IF EXISTS `pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pregunta` (
  `id_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `obligatoriedad` tinyint(1) NOT NULL,
  `tipo` int(11) NOT NULL,
  `encuesta` int(11) NOT NULL,
  PRIMARY KEY (`id_pregunta`),
  UNIQUE KEY `FK_id_encuesta` (`encuesta`),
  UNIQUE KEY `FK_tipo_pregunta` (`tipo`),
  CONSTRAINT `pregunta_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_pregunta` (`id_tipo_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pregunta_ibfk_2` FOREIGN KEY (`encuesta`) REFERENCES `encuesta` (`id_encuesta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pregunta`
--

LOCK TABLES `pregunta` WRITE;
/*!40000 ALTER TABLE `pregunta` DISABLE KEYS */;
/*!40000 ALTER TABLE `pregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pregunta_contestada`
--

DROP TABLE IF EXISTS `pregunta_contestada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pregunta_contestada` (
  `id_pregunta_contestada` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `resp_enc` int(11) NOT NULL,
  PRIMARY KEY (`id_pregunta_contestada`),
  UNIQUE KEY `FK_tipo_pregunta` (`tipo`),
  UNIQUE KEY `FK_id_resp_enc` (`resp_enc`),
  CONSTRAINT `pregunta_contestada_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_pregunta` (`id_tipo_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pregunta_contestada_ibfk_2` FOREIGN KEY (`resp_enc`) REFERENCES `encuesta_contestada` (`id_resp_enc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pregunta_contestada`
--

LOCK TABLES `pregunta_contestada` WRITE;
/*!40000 ALTER TABLE `pregunta_contestada` DISABLE KEYS */;
/*!40000 ALTER TABLE `pregunta_contestada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pregunta`
--

DROP TABLE IF EXISTS `tipo_pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pregunta` (
  `id_tipo_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `cant_max_respuesta` int(11) NOT NULL,
  PRIMARY KEY (`id_tipo_pregunta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pregunta`
--

LOCK TABLES `tipo_pregunta` WRITE;
/*!40000 ALTER TABLE `tipo_pregunta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_pregunta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-20 11:44:35
